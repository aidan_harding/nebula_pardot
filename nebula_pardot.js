var Nebula_Pardot = (function () {
	var _originalOptions = {};

	var _processDependentPicklist = function(masterField, childField, dependencies) {
		$('p.' + childField + '.pd-select select option').detach();
		var theseNewChildOptions = dependencies[getValueFromPardotField(masterField)];
		if(typeof theseNewChildOptions != "undefined") {
			for (var i = theseNewChildOptions.length - 1; i >= 0; i--) {
				var thisChildOptionNode = $.grep(_originalOptions[childField], function(testVal) {
					return $(testVal).text() === theseNewChildOptions[i];
				});
				if(thisChildOptionNode.length > 0) {
					$('p.' + childField + '.pd-select select').append(thisChildOptionNode[0]);
				}
			}
		}
	};

	var _processDependentCheckboxes = function(masterField, childFieldParagraph$, dependencies) {
		childFieldParagraph$.find('input[type=checkbox]').parent().hide();

		var theseNewChildOptions = dependencies[getValueFromPardotField(masterField)];

		if(typeof theseNewChildOptions != "undefined") {
			childFieldParagraph$.find('input[type=checkbox]').next().each(function(idx, value) {
				var value$ = $(value);
				if(theseNewChildOptions.indexOf(value$.text()) >= 0) {
					value$.parent().show();
				}
			});
		}
	};

	var dependentPicklist = function (masterField, childField, dependencies) {
		var childFieldParagraph$ = $('p.' + childField);
		if(childFieldParagraph$.hasClass('pd-select')) {
			_originalOptions[childField] = $('p.' + childField + '.pd-select select option').toArray();
			$('p.' + masterField + '.pd-select select').change(function(){
				_processDependentPicklist(masterField, childField, dependencies);
			});
			_processDependentPicklist(masterField, childField, dependencies);
		} else if(childFieldParagraph$.hasClass('pd-checkbox')) {
			$('p.' + masterField + '.pd-select select').change(function(){
				_processDependentCheckboxes(masterField, childFieldParagraph$, dependencies);
			});
			_processDependentCheckboxes(masterField, childFieldParagraph$, dependencies);
		}
	};

	var getValueFromPardotField = function(fieldName) {
		var fieldParagraph$ = $('p.' + fieldName);
		if(fieldParagraph$.hasClass('pd-radio')) {
			return fieldParagraph$.find('input[type=radio]:checked').next('label').text();
		} else if(fieldParagraph$.hasClass('pd-select')) {
			return fieldParagraph$.find('option:selected').prop('label');
		} else if(fieldParagraph$.hasClass('pd-text')) {
			return fieldParagraph$.find('input[type=text]').val();
		} else if(fieldParagraph$.hasClass('pd-checkbox')) {
			var checkboxes$ = fieldParagraph$.find('input[type=checkbox]');
			if(checkboxes$.length > 1) {
				return checkboxes$.map(function(index, cb) { 
					return {
						label : $(cb).next().text(),
						value : $(cb).prop('checked')
					}; 
				}).get();
			} else {
				return checkboxes$.prop('checked');
			}
			
		}
		return undefined;
	};

	var _processShowHide = function(controllingField, showValues, controlledField) {
		var val = getValueFromPardotField(controllingField);
		var doShow;

		if(Array.isArray(showValues)) {
			doShow = showValues.indexOf(val) >= 0;
		} else if(typeof showValues === "function") {
			doShow = showValues(val);
		} else {
			doShow = showValues === val;
		}

		if(doShow) {
			$('p.' + controlledField).show();
		} else {
			$('p.' + controlledField).hide();			
		}
	};

	var showHide = function(controllingField, showValues, controlledField) {
		$('p.' + controllingField + ' input').change(function() {
			_processShowHide(controllingField, showValues, controlledField)
		});

		_processShowHide(controllingField, showValues, controlledField);
	};
	
	var _syncCheckboxToRadios = function (newCheck$, yesRadio$, noRadio$) {
		var theRadio$;
	    if(newCheck$.prop('checked')) {
	        theRadio$ = yesRadio$;
	    } else {
	        theRadio$ = noRadio$;
	    }
	    theRadio$.prop('checked', true);
	};

	var radioToCheckbox = function(fieldName) {
		var radioParagraph$ = $('p.' + fieldName + '.pd-radio');

        var yesRadio$ = $('#' + radioParagraph$.find('label:contains("true")').attr('for'));
        var noRadio$ = $('#' + radioParagraph$.find('label:contains("false")').attr('for'));

        if(yesRadio$.length > 0 && noRadio$.length > 0) {
            radioParagraph$.children('span.value').hide();
            var newCheck$ = $('<input type="checkbox"></input>');

            if(yesRadio$.prop('checked')) {
                newCheck$.prop('checked', true);
            }

            newCheck$.change(function() {
                _syncCheckboxToRadios(newCheck$, yesRadio$, noRadio$);
            });
            radioParagraph$.append(newCheck$);
        }
	};
	var _doSyncCheckboxToText = function (checkboxFieldName, textFieldName) {
		var newTextVal = '';

		$('p.' + checkboxFieldName +'.pd-checkbox input[type=checkbox]:checked')
			.next('label')
			.each(function(idx, val) {
				newTextVal += (idx > 0 ? ';' : '') + $(val).text();
			});

		$('p.' + textFieldName + ' input').val(newTextVal);
	};
	var syncCheckboxToText = function(checkboxFieldName, textFieldName) {
		$('p.' + checkboxFieldName + '.pd-checkbox input[type=checkbox]')
			.each(function (idx, val) { 
				$(val).change(function() {_doSyncCheckboxToText(checkboxFieldName, textFieldName)});
		});
		_doSyncCheckboxToText(checkboxFieldName, textFieldName);
	};

	var replaceDynamicContentTags = function() {
		$('div[data-dynamic-content-tag]').each(
			function(_, thisDiv) {
				var thisDiv$ = $(thisDiv);
				var thisDynamicContentTag = thisDiv$.attr('data-dynamic-content-tag');
				var toReplace$ = $('*').filter(function(_, thisElement){
					if($(thisElement).text().indexOf(thisDynamicContentTag) >= 0) {
						return true;
					} else {
						var found = false;
						$(thisElement.attributes).each(function (_, thisAttribute) {
							found = found || thisAttribute.value.indexOf(thisDynamicContentTag) >= 0
						});
						return found;
					}
				}).last();
				toReplace$.text(toReplace$.text().replace(thisDynamicContentTag, thisDiv$.text()));
				if(toReplace$.length > 0) {
					$(toReplace$[0].attributes).each(function(_, thisAttribute) {
						if(typeof thisAttribute.value !== 'undefined') {
							thisAttribute.value = thisAttribute.value.replace(thisDynamicContentTag, thisDiv$.text());
						}
					});
				}
			});
	};

	return {
		/** Allows one picklist/select field (the master) to control the values available in another
		* picklist/select field. Also works for a checkboxes as the child field.
		*
		* @param masterField the name of the master field as a string
		* @param childField the name of the child field as a string
		* @param dependencies an object mapping values in the master field to lists of values in the child field
		*  
		* Example: 
		*    Nebula_Pardot.dependentPicklist("Organisation_Type", "Organisation_Industry", { "Allocator": ["Allocator/Investor"], "Government Trade Organisation": ["Wealth Management", "RIA"] } )
		*
		* In this example, we set the master field to be Organisation_Type and the child to be Organisation_Industry. For the dependencies:
		* "Allocator" in the master list should show just "Allocator/Investor" in the child. "Government Trade Organisation" should show
		* "Wealth Management" and "RIA".
		*
		* Selecting a value in the master list which is not mapped in the dependencies will result in the child select list being empty. Including
		* non-existent values for the child list in the dependencies will have no effect (the non-existent values will not be show in the child list).
		*/
		dependentPicklist: dependentPicklist,
		/** Gets the value of a field based on the Pardot name, abstracting away the details of how to get the value from the various types:
		* radio, select, text, checkbox. All fields except checkbox return text. A single checkbox returns true or false (boolean, not string).
		* multiple checkboxes return an array of mapped values. Each item in the array is of the form {label: "A checkbox label", value: true}
		*
		* @param fieldName the name of the field in Pardot
		*
		* Example:
		*    getValueFromPardotField("Brand_Family")
		*
		* Returns the value for the field Brand_Family.
		*/
		getValueFromPardotField : getValueFromPardotField,
		/** Show/hide one field based on the value of another
		*
		* @param controllingField the field whose values need to be checked
		* @param showValues an array, a single value, or a function returning a boolean which is used to show/hide the controlledField
		* @param controlledField the field to show/hide
		*  
		* Where showHide is a function, it should accept the result of getValueFromPardotField(controllingField) and return a boolean.
		*
		* Simple Examples: 
		*    Nebula_Pardot.showHide("Brand_Family", ["HFM", "CTA Intelligence"], "phone")
		*    Nebula_Pardot.showHide("Brand_Family", "HFM", "phone")
		*
		* Function Example: 
		* var freeTrialSelector = function(checkboxValues) { 
		* 	for(var i=0; i < checkboxValues.length; i++) {
		* 		if(checkboxValues[i].value) {
		* 			return true;
		*         }
		*     }
		* 	return false;
		* };
		* Nebula_Pardot.showHide("Request_a_free_trial", freeTrialSelector, "country");
		*
		* In the first example, the "phone" field will only be visible when "Brand_Family" is either "HFM" or "CTA Intelligence". In the second
		* example, it will only be visible when "Brand_Family" is "HFM".
		*/
		showHide : showHide,
		/** Converts a pair of radios with values true/false into a checkbox in the UI
		*
		* @param fieldName the name of the radio field in Pardot
		*
		* Example:
		*    radioToCheckbox("Privacy_Policy_Opt_In")
		*
		* Turns the field Privacy_Policy_Opt_In from two radios into a single checkbox
		*/
		radioToCheckbox : radioToCheckbox,
		/** The value of a multi-checkbox in a form is given as a single value by Pardot.
		* This method allows you to sync all of the checkboxes to a (hidden?) text field so
		* that you can use the merge-field of the text field to be able to see all of the selected
		* values. The text field will end up with all checked options labels, separated by semicolons.
		*
		* @param checkboxFieldName the name of the checkbox field in Pardot
		* @param textFieldName the name of the text field in Pardot
		*
		* Example:
		*    syncCheckboxToText('Secretaries__Document_Production_and_Support_Staff', 'Secretaries__Document_Production_and_Support_H')
		*
		* Syncs the checkboxes called Secretaries__Document_Production_and_Support_Staff to a text field called
		* Secretaries__Document_Production_and_Support_H.
		*/
		syncCheckboxToText : syncCheckboxToText,
		/** Pardot dynamic content can be places in various parts of a page, but it provides you with a script to load
		 * the content. That script cannot be used in places like attributes (e.g. button labels). To work around this
		 * include your script in a tag like this:
		 *
		 * <div data-dynamic-content-tag="$$Unsubscribe_Link_Text$$">
		 *   <script type="text/javascript" src="https://www.axis-communications.com/dcjs/133061/1158/dc.js"></script>
		 * </div>
		 *
		 * This indicates that you want to use $$Unsubscribe_Link_Text$$ as a tag name, and the content of that div
		 * is the data you want to merge into the tag.
		 *
		 * After that, just call replaceDynamicContentTags() after document load, and all the dynamic content will get
		 * placed correctly.
		 * 
		 */
		replaceDynamicContentTags : replaceDynamicContentTags
	};

})();