# nebula_pardot.js #

These are Javascript tools to add functionality to Pardot forms.

**You should not include this directly in your Pardot templates.** Download it, and add it to Files in Pardot. This isolates the customers from us changing that file without testing their system first.

One you have the file available, just include it like this in all the templates that require it:

```<script type="text/javascript" src="https://[YOUR PARDOT URL]/l/498481/2019-02-27/82xc6f/498481/281772/nebula_pardot.js"></script>```

Add the following JS to your layout template.

```
    document.addEventListener("DOMContentLoaded", () => {
       Nebula_Pardot.dependentPicklist("country", "state", { "Canada": ["Alberta", "Yukon"], "United States": ["Alabama", "Alaska"] } );
    });
```

Note: all values must exist in the picklist in the Form Field. 


# emit_size.js #
Pardot used to post a message with the rendered size of its document body, allowing a host page with Pardot in an iframe to resize itself appropriately and make the iframe seamless. 

This script replicates that functionality.

**You should not include this directly in your Pardot templates.** Download it, and add it to Files in Pardot. This isolates the customers from us changing that file without testing their system first.

One you have the file available, just include it like this in all the templates that require it:



```	<script type="text/javascript" src="/l/73142/2020-10-09/vm6g6v/73142/1602248058n61zNpea/emit_size.js"></script>```
	



Obviously, replace the URL with the one for your customer's copy of the file. 

They should not need to change their code on the host page at all. This should work the way Pardot used to. 