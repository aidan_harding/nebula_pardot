/**
 * Pardot used to post a message with the rendered size of its document body, allowing a host page with Pardot in an
 * iframe to resize itself appropriately and make the iframe seamless. This script replicates that functionality.
 *
 * @author aidan@nebulaconsulting.co.uk
 */

function postDocumentHeightMessage() {
    const style = getComputedStyle(document.body, null);

    window.parent.postMessage(
        document.body.scrollHeight + parseInt(style.marginTop) + parseInt(style.marginBottom),
        '*');
}
document.addEventListener("DOMContentLoaded", function() {
    new MutationObserver(postDocumentHeightMessage).observe(document.body, { attributes: true, childList: true, subtree: true });
    postDocumentHeightMessage();
});
